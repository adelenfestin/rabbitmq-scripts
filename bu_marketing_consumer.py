import pika

print "** Marketing Department **"

credentials = pika.PlainCredentials("guest", "guest")
conn_params = pika.ConnectionParameters("localhost",
                                        credentials = credentials)

conn_broker = pika.BlockingConnection(conn_params)
channel = conn_broker.channel()

# Company Fanout Exchange
# Company Direct Exchange
# Company Topic Exchange




channel.exchange_declare(exchange="company-fanout-exchange",
                         type="fanout")

channel.exchange_declare(exchange="company-direct-exchange",
                         type="direct")

channel.exchange_declare(exchange="company-direct-exchange",
                         type="topic")



channel.queue_declare(queue="marketing-dept-queue")

channel.queue_declare(queue="stock-manager-queue")

channel.queue_declare(queue="devops-queue")

channel.queue_declare(queue="info-queue")



# FANOUT
result = channel.queue_declare(auto_delete=True)
channel.queue_bind(queue=result.method.queue,
                   exchange='company-fanout-exchange')

# TOPIC
channel.queue_bind(queue='marketing-dept-queue',
                   exchange='company-topic-exchange',
                   routing_key='marketing.*')

channel.queue_bind(queue='stock-manager-queue',
                   exchange='company-topic-exchange',
                   routing_key='stock.*')

channel.queue_bind(queue='devops-queue',
                   exchange='company-topic-exchange',
                   routing_key='*.critical')

channel.queue_bind(queue='info-queue',
                   exchange='company-topic-exchange',
                   routing_key='*.critical')








def msg_consumer(channel, method, header, body):
        channel.basic_ack(delivery_tag=method.delivery_tag)
        if body == "quit":
                channel.basic_cancel(consumer_tag="hello-consumer")
                channel.stop_consuming()
        else:
                print body
        return


# Marketing Dept Consumer
def marketing_msg_consumer(channel, method, header, body):
        channel.basic_ack(delivery_tag=method.delivery_tag)
        if body == "quit":
                channel.basic_cancel(consumer_tag="hello-marketing")
                channel.stop_consuming()
        else:
                print body
        return




channel.basic_consume(msg_consumer,
                      queue="queue-auto-delete",
                      consumer_tag="hello-consumer-1")

channel.basic_consume(msg_consumer,
                      queue="queue-auto-delete-durable",
                      consumer_tag="hello-consumer-2")

channel.basic_consume(msg_consumer,
                      queue="queue-durable",
                      consumer_tag="hello-consumer-3")

channel.start_consuming()


