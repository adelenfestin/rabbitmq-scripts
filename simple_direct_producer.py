import pika, sys


credentials = pika.PlainCredentials("guest", "guest")
conn_params = pika.ConnectionParameters("localhost", credentials = credentials)

conn_broker = pika.BlockingConnection(conn_params)

channel = conn_broker.channel()

channel.exchange_declare()

channel.exchange_declare(exchange="direct-exchange-auto-delete",
                         type="direct",
                         passive=False,
                         durable=False,
                         auto_delete=True)

channel.exchange_declare(exchange="direct-exchange-auto-delete-durable",
                         type="direct",
                         passive=False,
                         durable=True,
                         auto_delete=True)

msg = sys.argv[1]

msg_props= pika.BasicProperties(delivery_mode = 2)
msg_props.content_type = "text/plain"

channel.basic_publish(body=msg,
                      exchange="direct-exchange-auto-delete",
                      properties=msg_props,
                      routing_key="hola")

channel.basic_publish(body=msg,
                      exchange="direct-exchange-auto-delete-durable",
                      properties=msg_props,
                      routing_key="hola")

channel.basic_publish(body=msg,
                      exchange="direct-exchange-auto-delete-durable",
                      properties=msg_props,
                      routing_key="hola")