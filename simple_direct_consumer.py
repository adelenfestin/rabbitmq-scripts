import pika

credentials = pika.PlainCredentials("guest", "guest")
conn_params = pika.ConnectionParameters("localhost",
                                        credentials = credentials)

conn_broker = pika.BlockingConnection(conn_params)
channel = conn_broker.channel()
channel.exchange_declare(exchange="direct-exchange-auto-delete",
                         type="direct",
                         passive=False,
                         durable=False,
                         auto_delete=True)

channel.exchange_declare(exchange="direct-exchange-auto-delete-durable",
                         type="direct",
                         passive=False,
                         durable=True,
                         auto_delete=True)

channel.queue_declare(queue="queue-auto-delete",
                      passive=False,
                      durable=False,
                      auto_delete=True)

channel.queue_declare(queue="queue-auto-delete-durable",
                      passive=False,
                      durable=True,
                      auto_delete=True)

channel.queue_declare(queue="queue-durable",
                      passive=False,
                      durable=True,
                      auto_delete=False)

channel.queue_declare(queue="queue-NOT-auto-delete-NOT-durable",
                      passive=False,
                      durable=False,
                      auto_delete=False)

channel.queue_bind(queue="queue-auto-delete",
                   exchange="direct-exchange-auto-delete",
                   routing_key="hola")

channel.queue_bind(queue="queue-auto-delete-durable",
                   exchange="direct-exchange-auto-delete-durable",
                   routing_key="hola")

channel.queue_bind(queue="queue-NOT-auto-delete-NOT-durable",
                   exchange="direct-exchange-auto-delete-durable",
                   routing_key="hola")

channel.queue_bind(queue="queue-durable",
                   exchange="direct-exchange-auto-delete-durable",
                   routing_key="hola")


def msg_consumer(channel, method, header, body):
        channel.basic_ack(delivery_tag=method.delivery_tag)
        if body == "quit":
                channel.basic_cancel(consumer_tag="hello-consumer")
                channel.stop_consuming()
        else:
                print body
        return

channel.basic_consume(msg_consumer,
                      queue="queue-auto-delete",
                      consumer_tag="hello-consumer-1")

channel.basic_consume(msg_consumer,
                      queue="queue-auto-delete-durable",
                      consumer_tag="hello-consumer-2")

channel.basic_consume(msg_consumer,
                      queue="queue-durable",
                      consumer_tag="hello-consumer-3")

channel.start_consuming()


