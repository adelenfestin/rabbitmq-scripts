import pika

credentials = pika.PlainCredentials("guest", "guest")
conn_params = pika.ConnectionParameters("localhost",
                                        credentials = credentials)


connection = pika.BlockingConnection(conn_params)

channel = connection.channel()

print "Deleting QUEUE queue-auto-delete"
channel.queue_delete(queue='queue-auto-delete')

print "Deleting QUEUE queue-auto-delete-durable"
channel.queue_delete(queue='queue-auto-delete-durable')

connection.close()